#include <stdio.h>
#define CROKEY_IMPL
#include "crokey.h"

int main(void) {
    enum crokey_key result;
    
    do {
        result = crokey_get_pressed_key();
    } while(result != KEY_SPACE);
    
    printf("%s was pressed", crokey_enum_to_string(result));
    
    return 0;
}
