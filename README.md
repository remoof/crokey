# crokey

Single header Cross-platform C library for getting pressed keys/buttons without needing a window.  
Supports Linux, Windows and macOS.  
Inspired by the rust library [device_query.](https://github.com/ostrosco/device_query)

## Dependencies

### Linux
X11 development libraries

pacman based: `libx11`

apt based: `libx11-dev`

dnf based: `xorg-x11-server-devel`

## How to use

### Including
In only one source file `#define CROKEY_IMPL` before including it.
All other files can just regularly include the header.

### Functions
`crokey_get_pressed_key(void)` returns the pressed key or button as an enum. Returns KEY_LIST_NONE if nothing is pressed.

`crokey_enum_to_string(enum crokey_key key)` returns a char* to the static string. Returns "None" if not valid.

[Examples](https://codeberg.org/remoof/crokey/src/branch/master/examples)

### Linking

Linux requires linking with `-lX11`

Windows requires linking with `-luser32`

macOS requires linking with `-framework AppKit`

## Known issues
macOS requires special permissions, explained [here](https://github.com/ostrosco/device_query/issues/34#issuecomment-791094051)
